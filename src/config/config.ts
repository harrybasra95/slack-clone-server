import * as dotenv from 'dotenv';

dotenv.config();

const config: Record<string, any> = {
     development: {
          env: process.env.NODE_ENV,
          PORT: process.env.PORT || 4000,
          defaultOTP: 12345,
          jwtSecret: process.env.JWT_SECRET,
          sessionSecret: process.env.SESSION_SECRET,
          db: {
               type: process.env.DEV_DB_TYPE,
               host: process.env.DEV_DB_HOST,
               username: process.env.DEV_DB_USER,
               password: process.env.DEV_DB_PASSWORD,
               port: process.env.DEV_DB_PORT,
               database: process.env.DEV_DB_DATABASE,
          },
     },
};

export default config[process.env.NODE_ENV || 'development'];
