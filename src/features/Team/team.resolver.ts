import MyContext from '@interfaces/context.interface';
import { GraphQLResolveInfo } from 'graphql';
import fieldsToRelations from 'graphql-fields-to-relations';
import {
     Arg,
     Authorized,
     Ctx,
     Info,
     Mutation,
     Query,
     Resolver,
} from 'type-graphql';
import Team from './Entity/team.entity';
import TeamCreateI from './Input/team.create';

@Resolver(Team)
export default class TeamResolver {
     @Query((_returns) => [Team], {
          nullable: true,
          description: 'Get details of all the Teams',
     })
     async teams(
          @Ctx()
          ctx: MyContext,
          @Info()
          info: GraphQLResolveInfo
     ): Promise<Team[]> {
          const relationPaths = fieldsToRelations(info);
          const foundTeams = await ctx.em
               .getRepository(Team)
               .findAll(relationPaths);
          return foundTeams;
     }

     @Query((_returns) => Team, {
          description: 'Get team by team id',
     })
     async teamById(
          @Ctx()
          ctx: MyContext,
          @Info()
          info: GraphQLResolveInfo,
          @Arg('id', (_type) => String)
          id: string
     ): Promise<Team> {
          const relationPaths = fieldsToRelations(info);
          const foundTeam = await ctx.em
               .getRepository(Team)
               .findOne(id, relationPaths);
          return foundTeam as Team;
     }

     @Authorized()
     @Mutation((_returns) => Team, {
          description: 'Create Team',
     })
     async createTeam(
          @Ctx()
          ctx: MyContext,
          @Arg('data', (_type) => TeamCreateI)
          data: TeamCreateI
     ): Promise<Team> {
          const teamRepository = ctx.em.getRepository(Team);
          const { name } = data;
          let team;
          team = await teamRepository.findOne({ name });
          if (team) throw 'Team already exists with this name';
          team = new Team(data);
          team.owner = ctx.user!;
          await teamRepository.persist(team).flush();
          return team as Team;
     }
}
