import Base from '@baseEntity';
import User from '@features/User/Entity/user.entity';
import {
     Collection,
     Entity,
     ManyToMany,
     ManyToOne,
     Property,
} from '@mikro-orm/core';
import { Field, ObjectType } from 'type-graphql';
import TeamCreateI from '../Input/team.create';

@Entity({ tableName: 'teams' })
@ObjectType({ description: 'The Team model' })
export default class Team extends Base<Team> {
     @Field((_type) => String)
     @Property()
     name: String;

     @Field((_type) => User)
     @ManyToOne(() => User, { joinColumn: 'ownerId' })
     owner!: User;

     @Field((_type) => String)
     ownerId: string;

     @Field((_type) => [User])
     @ManyToMany({
          entity: () => User,
          joinColumn: 'teamId',
          pivotTable: 'members',
     })
     users = new Collection<User>(this);

     constructor(body: TeamCreateI) {
          super(body);
     }
}
