import { IsString } from 'class-validator';
import { Field, InputType } from 'type-graphql';
import TeamCreateI from './team.create';

@InputType()
export class TeamUpdateI extends TeamCreateI {
     @Field()
     @IsString()
     id: string;
}
