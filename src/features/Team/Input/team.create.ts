import { IsString } from 'class-validator';
import { Field, InputType } from 'type-graphql';

@InputType()
export default class TeamCreateI {
     @Field()
     @IsString()
     name: String;
}
