import { IsBoolean, IsString } from 'class-validator';
import { Field, InputType } from 'type-graphql';

@InputType()
export default class ChannelCreateI {
     @Field()
     @IsString()
     name: string;

     @Field()
     @IsBoolean()
     public: string;
}
