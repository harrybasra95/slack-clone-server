import { IsString } from 'class-validator';
import { Field, InputType } from 'type-graphql';
import ChannelCreateI from './channel.create';

@InputType()
export class ChannelUpdateI extends ChannelCreateI {
     @Field()
     @IsString()
     id: string;
}
