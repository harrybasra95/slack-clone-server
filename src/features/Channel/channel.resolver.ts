import MyContext from '@interfaces/context.interface';
import { GraphQLResolveInfo } from 'graphql';
import fieldsToRelations from 'graphql-fields-to-relations';
import { Arg, Ctx, Info, Mutation, Query, Resolver } from 'type-graphql';
import Channel from './Entity/channel.entity';
import ChannelCreateI from './Input/channel.create';

@Resolver(Channel)
export default class ChannelResolver {
     @Query((_returns) => [Channel], {
          nullable: true,
          description: 'Get details of all the Channels',
     })
     async channels(
          @Ctx()
          ctx: MyContext,
          @Info()
          info: GraphQLResolveInfo
     ): Promise<Channel[]> {
          const relationPaths = fieldsToRelations(info);
          const foundChannels = await ctx.em
               .getRepository(Channel)
               .findAll(relationPaths);
          return foundChannels;
     }

     @Query((_returns) => Channel, {
          description: 'Get channel by channel id',
     })
     async channelById(
          @Ctx()
          ctx: MyContext,
          @Info()
          info: GraphQLResolveInfo,
          @Arg('id', (_type) => String)
          id: string
     ): Promise<Channel> {
          const relationPaths = fieldsToRelations(info);
          const foundChannel = await ctx.em
               .getRepository(Channel)
               .findOne(id, relationPaths);
          return foundChannel as Channel;
     }

     @Mutation((_returns) => Channel, {
          description: 'Create Channel',
     })
     async createChannel(
          @Ctx()
          ctx: MyContext,
          @Arg('data', (_type) => ChannelCreateI)
          data: ChannelCreateI
     ): Promise<Channel> {
          const channelRepository = ctx.em.getRepository(Channel);
          const { name } = data;
          let channel;
          channel = await channelRepository.findOne({ name });
          if (channel) throw 'Channel already exists with this name';
          channel = new Channel(data);
          await channelRepository.persist(channel).flush();
          return channel as Channel;
     }
}
