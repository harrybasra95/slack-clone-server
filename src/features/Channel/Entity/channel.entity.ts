import Base from '@baseEntity';
import Team from '@features/Team/Entity/team.entity';
import { ManyToOne } from '@mikro-orm/core';
import { Entity, Property } from '@mikro-orm/core';
import { Field, ObjectType } from 'type-graphql';
import ChannelCreateI from '../Input/channel.create';

@Entity({ tableName: 'channels' })
@ObjectType({ description: 'The Channel model' })
export default class Channel extends Base<Channel> {
     @Field((_type) => String)
     @Property()
     name: string;

     @Field((_type) => Boolean)
     @Property()
     public: Boolean;

     @Field((_type) => Team)
     @ManyToOne({ joinColumn: 'teamId' })
     team!: Team;

     @Field((_type) => String)
     teamId: string;

     constructor(body: ChannelCreateI) {
          super(body);
     }
}
