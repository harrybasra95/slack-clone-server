import Base from '@baseEntity';
import Team from '@features/Team/Entity/team.entity';
import {
     BeforeCreate,
     BeforeUpdate,
     Collection,
     Entity,
     ManyToMany,
     Property,
} from '@mikro-orm/core';
import { hashSync } from 'bcrypt';
import { Field, ObjectType } from 'type-graphql';
import UserCreateI from '../Input/user.create';

@Entity({ tableName: 'users' })
@ObjectType({ description: 'The User model' })
export default class User extends Base<User> {
     @Field((_type) => String)
     @Property()
     email: String;

     @Field((_type) => String)
     @Property()
     username: String;

     @Property()
     password: String;

     @Property()
     token: String;

     @Field((_type) => [Team])
     @ManyToMany({
          entity: () => Team,
          joinColumn: 'userId',
          inverseJoinColumn: 'teamId',
          pivotTable: 'members',
          fixedOrderColumn: 'id',
     })
     teams = new Collection<Team>(this);

     @BeforeUpdate()
     @BeforeCreate()
     setPassword() {
          if (this.password) {
               this.password = hashSync(this.password as string, 10);
          }
     }

     constructor(body: UserCreateI) {
          super(body);
     }
}
