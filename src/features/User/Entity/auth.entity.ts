import { Field, ObjectType } from 'type-graphql';
import User from './user.entity';

@ObjectType({ description: 'The User Auth model' })
export default class AuthUser {
     @Field()
     user: User;

     @Field({ nullable: true })
     token: string;
}
