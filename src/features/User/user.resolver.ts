import MyContext from '@interfaces/context.interface';
import { GraphQLResolveInfo } from 'graphql';
import fieldsToRelations from 'graphql-fields-to-relations';
import {
     Arg,
     Authorized,
     Ctx,
     Info,
     Mutation,
     Query,
     Resolver,
} from 'type-graphql';
import { createToken } from 'utils/helpers/token';
import AuthUser from './Entity/auth.entity';
import User from './Entity/user.entity';
import UserCreateI from './Input/user.create';

@Resolver(User)
export default class UserResolver {
     @Authorized()
     @Query((_returns) => [User], {
          nullable: true,
          description: 'Get details of all the Users',
     })
     async users(
          @Ctx()
          ctx: MyContext,
          @Info()
          info: GraphQLResolveInfo
     ): Promise<User[]> {
          const relationPaths = fieldsToRelations(info);
          const foundUsers = await ctx.em
               .getRepository(User)
               .findAll(relationPaths);
          return foundUsers;
     }

     @Authorized()
     @Query((_returns) => User, {
          description: 'Get user by user id',
     })
     async userById(
          @Ctx()
          ctx: MyContext,
          @Info()
          info: GraphQLResolveInfo,
          @Arg('id', (_type) => String)
          id: string
     ): Promise<User> {
          const relationPaths = fieldsToRelations(info);
          const foundUser = await ctx.em
               .getRepository(User)
               .findOne(id, relationPaths);
          return foundUser as User;
     }
     @Mutation((_returns) => AuthUser, {
          description: 'Create user',
     })
     async createUser(
          @Ctx()
          ctx: MyContext,
          @Arg('data', (_type) => UserCreateI)
          data: UserCreateI
     ): Promise<AuthUser> {
          const userRepository = ctx.em.getRepository(User);
          const { email } = data;
          let user;
          user = await userRepository.findOne({ email });
          if (user) throw 'User already exists with this email';
          user = new User(data);
          user.token = createToken(user);
          user.assign(user);
          await userRepository.persist(user).flush();
          return { user, token: user.token } as AuthUser;
     }
     // @Mutation((_return) => AuthUser, {
     //      description: 'Verify user ',
     // })
     // async verifyUser(
     //      @Ctx()
     //      ctx: MyContext,
     //      @Arg('userId', (_type) => String)
     //      userId: String,
     //      @Arg('code', (_type) => Number)
     //      code: Number
     // ): Promise<AuthUser> {
     //      const userRepository = ctx.em.getRepository(User);
     //      const user = await userRepository.findOne(userId as string);
     //      if (!user) throw 'User not found';
     //      if (config.env === 'production' && code !== user.activationCode)
     //           throw 'Invalid OTP';
     //      if (
     //           config.env === 'development' &&
     //           code !== user.activationCode &&
     //           code !== config.defaultOTP
     //      )
     //           throw 'Invalid OTP';
     //      user.token = createToken(user.id);
     //      user.activationCode = null;
     //      user.status = UserStatues.ACTIVE;
     //      user.assign(user);
     //      await userRepository.persist(user).flush();
     //      return { user, token: user.token } as AuthUser;
     // }
     // @Mutation((_returns) => User, {
     //      description: 'Update user details',
     // })
     // async updateUser(
     //      @Ctx()
     //      ctx: MyContext,
     //      @Info()
     //      info: GraphQLResolveInfo,
     //      @Arg('data', (_type) => UserUpdateI)
     //      data: UserUpdateI
     // ): Promise<User> {
     //      const relationPaths = fieldsToRelations(info);
     //      const userRepository = ctx.em.getRepository(User);
     //      const foundUser = await userRepository.findOne(
     //           data.id,
     //           relationPaths
     //      );
     //      if (!foundUser) throw 'User not found';
     //      foundUser.assign(data);
     //      await userRepository.persist(foundUser).flush();
     //      return foundUser as User;
     // }
     // @Mutation((_returns) => User, {
     //      description: 'Forgot password route',
     // })
     // async forgotPassword(
     //      @Ctx()
     //      ctx: MyContext,
     //      @Info()
     //      info: GraphQLResolveInfo,
     //      @Arg('email', (_type) => String)
     //      email: string
     // ): Promise<User> {
     //      const relationPath = fieldsToRelations(info);
     //      const foundUser = await ctx.em
     //           .getRepository(User)
     //           .findOne(email, relationPath);
     //      if (!foundUser) throw 'User is not registered with us';
     //      if (foundUser.loginType !== LoginTypes.EMAIL)
     //           throw 'You have logged in using social media';
     //      foundUser.activationCode = generateOTP();
     //      sendOTPOnEmail(foundUser, foundUser.activationCode);
     //      await ctx.em.getRepository(User).persist(foundUser).flush();
     //      return foundUser as User;
     // }
     // @Mutation((_returns) => User, {
     //      description: 'Change password for user',
     // })
     // async changePassword(
     //      @Ctx()
     //      ctx: MyContext,
     //      @Arg('oldPassword', (_type) => String, { nullable: true })
     //      oldPassword: string,
     //      @Arg('newPassword', (_type) => String)
     //      newPassword: string,
     //      @Arg('userId', (_type) => String)
     //      userId: string
     // ): Promise<User> {
     //      const foundUser = await ctx.em.getRepository(User).findOne(userId);
     //      if (!foundUser) throw 'User not found';
     //      if (foundUser.loginType !== LoginTypes.EMAIL)
     //           throw 'You have logged in using social media';
     //      if (oldPassword) {
     //           const isPassMatch = comparePassword(
     //                oldPassword,
     //                foundUser.password
     //           );
     //           if (!isPassMatch) throw 'Old password is incorrect';
     //      }
     //      foundUser.password = newPassword;
     //      await ctx.em.getRepository(User).persist(foundUser).flush();
     //      return foundUser as User;
     // }
     // @Mutation((_returns) => String, {
     //      description: 'Resend new otp to email address',
     // })
     // async resend(
     //      @Ctx()
     //      ctx: MyContext,
     //      @Arg('userId', (_type) => String)
     //      userId: string
     // ): Promise<String> {
     //      const foundUser = await ctx.em.getRepository(User).findOne(userId);
     //      if (!foundUser) throw 'User not found';
     //      if (foundUser.loginType !== LoginTypes.EMAIL)
     //           throw 'You have logged in using social media';
     //      foundUser.activationCode = generateOTP();
     //      sendOTPOnEmail(foundUser, foundUser.activationCode);
     //      await ctx.em.getRepository(User).persist(foundUser).flush();
     //      return 'Otp sent successfully';
     // }
}
