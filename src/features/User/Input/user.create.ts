import { IsEmail, IsString } from 'class-validator';
import { Field, InputType } from 'type-graphql';

@InputType()
export default class UserCreateI {
     @Field()
     @IsEmail()
     email: String;

     @Field()
     @IsString()
     username: String;

     @Field()
     @IsString()
     password: String;
}
