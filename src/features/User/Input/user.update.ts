import { IsString } from 'class-validator';
import { Field, InputType } from 'type-graphql';
import UserCreateI from './user.create';

@InputType()
export class UserUpdateI extends UserCreateI {
     @Field()
     @IsString()
     id: string;
}
