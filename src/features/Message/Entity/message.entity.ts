import Base from '@baseEntity';
import Channel from '@features/Channel/Entity/channel.entity';
import User from '@features/User/Entity/user.entity';
import { Entity, ManyToOne, Property } from '@mikro-orm/core';
import { Field, ObjectType } from 'type-graphql';
import MessageCreateI from '../Input/message.create';

@Entity({ tableName: 'messages' })
@ObjectType({ description: 'The Message model' })
export default class Message extends Base<Message> {
     @Field((_type) => String)
     @Property()
     text: String;

     @Field((_type) => User)
     @ManyToOne({ joinColumn: 'userId' })
     user!: User;

     @Field((_type) => String)
     userId: string;

     @Field((_type) => Channel)
     @ManyToOne({ joinColumn: 'channelId' })
     channel!: Channel;

     @Field((_type) => String)
     channelId: string;

     constructor(body: MessageCreateI) {
          super(body);
     }
}
