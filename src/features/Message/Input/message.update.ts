import { IsString } from 'class-validator';
import { Field, InputType } from 'type-graphql';
import MessageCreateI from './message.create';

@InputType()
export class MessageUpdateI extends MessageCreateI {
     @Field()
     @IsString()
     id: string;
}
