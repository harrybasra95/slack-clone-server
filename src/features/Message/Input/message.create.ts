import { IsString } from 'class-validator';
import { Field, InputType } from 'type-graphql';

@InputType()
export default class MessageCreateI {
     @Field()
     @IsString()
     text: String;
}
