import { Migration } from '@mikro-orm/migrations';

export class Migration20210729041414 extends Migration {

  async up(): Promise<void> {
    this.addSql('create table `users` (`id` varchar(255) not null, `createdAt` datetime not null, `updatedAt` datetime not null, `email` varchar(255) not null, `username` varchar(255) not null, `password` varchar(255) not null) default character set utf8mb4 engine = InnoDB;');
    this.addSql('alter table `users` add primary key `users_pkey`(`id`);');

    this.addSql('create table `teams` (`id` varchar(255) not null, `createdAt` datetime not null, `updatedAt` datetime not null, `name` varchar(255) not null, `ownerId` varchar(255) not null) default character set utf8mb4 engine = InnoDB;');
    this.addSql('alter table `teams` add primary key `teams_pkey`(`id`);');
    this.addSql('alter table `teams` add index `teams_ownerId_index`(`ownerId`);');

    this.addSql('create table `members` (`id` int unsigned not null auto_increment primary key, `userId` varchar(255) not null, `teamId` varchar(255) not null) default character set utf8mb4 engine = InnoDB;');
    this.addSql('alter table `members` add index `members_userId_index`(`userId`);');
    this.addSql('alter table `members` add index `members_teamId_index`(`teamId`);');

    this.addSql('create table `channels` (`id` varchar(255) not null, `createdAt` datetime not null, `updatedAt` datetime not null, `name` varchar(255) not null, `public` tinyint(1) not null, `teamId` varchar(255) not null) default character set utf8mb4 engine = InnoDB;');
    this.addSql('alter table `channels` add primary key `channels_pkey`(`id`);');
    this.addSql('alter table `channels` add index `channels_teamId_index`(`teamId`);');

    this.addSql('create table `messages` (`id` varchar(255) not null, `createdAt` datetime not null, `updatedAt` datetime not null, `text` varchar(255) not null, `userId` varchar(255) not null, `channelId` varchar(255) not null) default character set utf8mb4 engine = InnoDB;');
    this.addSql('alter table `messages` add primary key `messages_pkey`(`id`);');
    this.addSql('alter table `messages` add index `messages_userId_index`(`userId`);');
    this.addSql('alter table `messages` add index `messages_channelId_index`(`channelId`);');

    this.addSql('alter table `teams` add constraint `teams_ownerId_foreign` foreign key (`ownerId`) references `users` (`id`) on update cascade;');

    this.addSql('alter table `members` add constraint `members_userId_foreign` foreign key (`userId`) references `users` (`id`) on update cascade on delete cascade;');
    this.addSql('alter table `members` add constraint `members_teamId_foreign` foreign key (`teamId`) references `teams` (`id`) on update cascade on delete cascade;');

    this.addSql('alter table `channels` add constraint `channels_teamId_foreign` foreign key (`teamId`) references `teams` (`id`) on update cascade;');

    this.addSql('alter table `messages` add constraint `messages_userId_foreign` foreign key (`userId`) references `users` (`id`) on update cascade;');
    this.addSql('alter table `messages` add constraint `messages_channelId_foreign` foreign key (`channelId`) references `channels` (`id`) on update cascade;');
  }

}
