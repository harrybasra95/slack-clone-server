import { Migration } from '@mikro-orm/migrations';

export class Migration20210729053325 extends Migration {

  async up(): Promise<void> {
    this.addSql('alter table `users` add `token` varchar(255) not null;');
  }

}
