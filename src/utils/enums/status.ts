import { registerEnumType } from 'type-graphql';

enum Status {
     ACTIVE = 'active',
     INACTIVE = 'inactive',
     BLOCKED = 'blocked',
     DELETED = 'deleted',
}

registerEnumType(Status, {
     name: 'Status',
     description: 'General model status',
});

export default Status;
