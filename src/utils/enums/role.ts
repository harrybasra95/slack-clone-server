import { registerEnumType } from 'type-graphql';

export enum Roles {
     USER = 'user',
     ARTIST = 'artist',
     EVENT_MANAGER = 'eventManager',
     ADMIN = 'admin',
}

registerEnumType(Roles, {
     name: 'Roles',
     description: 'User roles',
});
