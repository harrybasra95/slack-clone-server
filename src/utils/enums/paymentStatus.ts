import { registerEnumType } from 'type-graphql';

export enum PaymentStatus {
     PENDING = 'pending',
     PAID = 'paid',
     FAILED = 'failed',
     CANCELLED = 'cancelled',
     REFUNDED = 'refunded',
}

registerEnumType(PaymentStatus, {
     name: 'PaymentStatus',
     description: 'Payment status types',
});
