import { registerEnumType } from 'type-graphql';

export enum UserStatues {
     PENDING = 'pending',
     ACTIVE = 'active',
     INACTIVE = 'inactive',
     BLOCKED = 'blocked',
     DELETED = 'deleted',
}

registerEnumType(UserStatues, {
     name: 'UserStatues',
     description: 'User account status',
});
