import { registerEnumType } from 'type-graphql';

export enum ArtistInvite {
     INVITED = 'invited',
     ACCEPTED = 'accepted',
     REJECTED = 'rejected',
     REJECTED_EM = 'rejectedByEventManager',
}

registerEnumType(ArtistInvite, {
     name: 'ArtistInvite',
     description: 'Artist invite status types',
});
