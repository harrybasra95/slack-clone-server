import { registerEnumType } from 'type-graphql';

export enum EventStatus {
     TEST = 'test',
     STARTED = 'started',
     PAUSED = 'paused',
     ENDED = 'ended',
}

registerEnumType(EventStatus, {
     name: 'EventStatus',
     description: 'Event status types',
});
