import { registerEnumType } from 'type-graphql';

export enum EventState {
     UPCOMING = 'upcoming',
     ONGOING = 'ongoing',
     CANCELLED = 'cancelled',
     COMPLETED = 'completed',
}

registerEnumType(EventState, {
     name: 'EventState',
     description: 'Event state types',
});
