import { Field, ObjectType } from 'type-graphql';

@ObjectType()
export default class Coordinates {
     @Field()
     latitude: number;
     @Field()
     longitude: number;
}
