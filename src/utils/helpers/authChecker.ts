import User from '@features/User/Entity/user.entity';
import MyContext from '@interfaces/context.interface';
import TokenI from '@interfaces/token.interface';
import { AuthChecker } from 'type-graphql';
import { verifyToken } from './token';

const customAuthChecker: AuthChecker<MyContext> = async ({ context }) => {
     const tokenName = 'x-access-token';
     const token = context.req.headers[tokenName]?.toString();
     if (!token) throw new Error('Token is required');
     const { id } = verifyToken(token) as TokenI;

     if (!id) throw Error('Invalid token');
     const foundUser: User | null = await context.em
          .getRepository(User)
          .findOne({ id });
     if (!foundUser) throw Error('Invalid Token');
     context.user = foundUser;
     return true;
};

export default customAuthChecker;
