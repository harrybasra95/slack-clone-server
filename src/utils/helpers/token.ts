import config from '@config';
import jwt from 'jsonwebtoken';

export const verifyToken = (token: string) => {
     return jwt.verify(token, config.jwtSecret, {
          ignoreExpiration: true,
     });
};

export const createToken = (entity: any): string => {
     const claims = {
          id: entity.id,
     };
     return jwt.sign(claims, config.jwtSecret, {
          expiresIn: 86400,
     });
};
